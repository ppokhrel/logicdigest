from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

STATUS = (
    (0,"Draft"),
    (1,"Publish")
)

# Create your models here.
class Post (models.Model):
    title = models.CharField(max_length = 200, unique = True)
    slug = models.SlugField(max_length=200, unique = True)
    author = models.ForeignKey(User, on_delete= models.CASCADE, related_name= 'blog_posts')
    updated_on = models.DateTimeField(auto_now= True)
    content = models.TextField ()   
    created_on = models.DateTimeField(auto_now_add= True)
    status = models.IntegerField(choices= STATUS, default= 0)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title

    def clean(self):
        if not len(self.title) >5:
            raise ValidationError(
                {'title': "Title should have at least 10 characters(letters or numbers"}
            )
    
    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    


class Comment(models.Model):
    post = models.ForeignKey(Post,on_delete = models.CASCADE, related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ['created_on']

    def __str__(self):
        return 'Comment {} by {}'.format(self.body, self.name)

