from . import views
from django.urls import include

from django.urls import path

urlpatterns = [
    path('', views.index, name= 'home'),
    path("blogs/", views.PostList.as_view(), name = "blog_list"),
    path("<slug:slug>/", views.post_detail, name = "post_detail"),
    path("about_us",views.about_us, name= "about_us"),

]
